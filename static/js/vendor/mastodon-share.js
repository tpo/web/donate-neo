// share to Mastodon, prompt for user's instance
const mastodon_link = document.querySelector('.mastodon-share');
mastodon_link.addEventListener('click', (e) => {
  let instance = window.prompt(
    'Please enter your Mastodon instance',
    'mastodon.social'
  );
  if (!instance) {
    e.preventDefault();
  } else {
    // strip http:// or http:// if they prepend the URL with it themselves
    instance = instance.replace(/^(https?:|)\/\//, "")
    mastodon_link.href = 'https://' + instance + '/share?url=https%3A//donate.torproject.org/&text=I%20just%20donated%20to%20the%20Tor%20Project%20because%20I%20support%20the%20human%20right%20to%20privacy.';
  }
});