import logging

from django.apps import AppConfig

from . import container, settings


class TordonateConfig(AppConfig):
    name = "tordonate"

    def ready(self) -> None:
        container.wire(
            modules=[
                ".forms",
                ".middleware",
                ".views",
                ".stripe.views",
                ".stripe.controller",
                ".paypal.views",
                ".civicrm.forms",
                ".civicrm.repository",
                ".civicrm.views",
            ]
        )
        logger = logging.getLogger(__package__)
        logger.info("Tor Donate application starting in the %s environment.", settings.APP_ENV)
