"""tordonate URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.conf import settings
from django.conf.urls.static import static
from django.urls import include, path
from django_ratelimit.decorators import ratelimit  # type: ignore

from .civicrm import urls as civi_urls
from .paypal import urls as paypal_urls
from .stripe import urls as stripe_urls
from .views import (AltchaView, CryptocurrencyView, DonateFormView, DonateThankYouView, FAQView,
                    PrivacyPolicyView, RefundPolicyView, StateDisclosuresView, TemplateView,
                    redirect_view)

urlpatterns = [
    path(
        "",
        # ratelimit() is employed here to ensure neither the donate form nor the subdomain itself
        # is affected by HTTP flooding attacks. Details here:
        # https://django-ratelimit.readthedocs.io/en/stable/usage.html
        ratelimit(
            key='ip',
            method=("GET", "POST"),
            rate=settings.RATE_LIMITING_RATE
        )(DonateFormView.as_view()),
        name="donate_form",
    ),
    path("validate/", DonateFormView.ajax_validate, name="donate_form_validation"),
    path("cryptocurrency/", CryptocurrencyView.as_view(), name="cryptocurrency"),
    path("donate-thank-you/", DonateThankYouView.as_view(), name="donate_thank_you"),
    path("faq/", FAQView.as_view(), name="faq"),
    path("privacy-policy/", PrivacyPolicyView.as_view(), name="privacy_policy"),
    path("state-disclosures/", StateDisclosuresView.as_view(), name="state_disclosures"),
    path("refund-policy/", RefundPolicyView.as_view(), name="refund_policy"),
    path("captcha/", include("captcha.urls")),
    path("", include('django_prometheus.urls')),
    path("", include(civi_urls)),
    path("", include(paypal_urls)),
    path("stripe/", include(stripe_urls)),
    path("challenge/", AltchaView().serve_challenge),  # type: ignore
    path("verifychallenge/", AltchaView().verify_challenge),  # type: ignore
    path(
        "robots.txt",
        TemplateView.as_view(template_name="robots.txt.jinja", content_type="text/plain"),
    ),

    # permanent redirects
    path("champions-of-privacy/", redirect_view),
    path("donate-faq/", redirect_view, {"destpath": "/faq/"}),
    path("monthly-giving/", redirect_view),
]

# error pages
handler404 = "tordonate.views.handler404"
handler500 = "tordonate.views.handler500"

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
