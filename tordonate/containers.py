# from uuid import uuid4

from dependency_injector import containers, providers
from django.conf import settings

from .civicrm.repository import CivicrmRepositoryMock
from .paypal import PaypalController
from .redis import RedisController
from .stripe import StripeController


class Container(containers.DeclarativeContainer):
    redis: providers.Factory[RedisController] = providers.Factory(
        RedisController,
        settings.REDIS_SERVER,
        settings.REDIS_PORT,
        settings.REDIS_DB,
        settings.REDIS_PASSWORD,
        settings.APP_ENV,
    )
    civi: providers.Factory[CivicrmRepositoryMock] = providers.Factory(
        CivicrmRepositoryMock,
        redis=redis,
        yec_total=1000000,
        is_yec=settings.IS_YEC,
        minimum_donation=settings.MINIMUM_DONATION,
    )
    paypal: providers.Factory[PaypalController] = providers.Factory(
        PaypalController,
        settings.PAYPAL_CLIENT_ID,
        settings.PAYPAL_APP_SECRET,
        settings.PAYPAL_SUBSCRIPTION_PRODUCT_ID,
        settings.PAYPAL_WEBHOOK_ID,
        settings.TRACE_PAYPAL_WEBHOOK,
        civi=civi,
    )
    stripe: providers.Factory[StripeController] = providers.Factory(
        StripeController,
        settings.STRIPE_API_KEY,
        settings.STRIPE_API_SECRET,
        settings.STRIPE_WEBHOOK_SECRET,
        settings.TRACE_STRIPE_WEBHOOK,
        civi=civi,
    )
