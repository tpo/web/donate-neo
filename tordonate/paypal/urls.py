from django.urls import path

from . import views

urlpatterns = [
    path("paypal/create-order/", views.create_order, name="create_order"),
    path(
        "paypal/capture-order-payment/",
        views.capture_order_payment,
        name="capture_order_payment",
    ),
    path("paypal/create-subscription/", views.create_subscription, name="create_subscription"),
    path("paypal/capture-subscription/", views.capture_subscription, name="capture_subscription"),
    path("paypal-ipn", views.ipn, name="ipn"),
    path("paypal/webhook/", views.webhook, name="webhook"),
]
