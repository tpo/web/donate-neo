from prometheus_client import Counter

RateLimiter = Counter(
    "donate_rate_limiter",
    "Rate limiting triggered",
    ["type", "method"]
)
# initialize the counter so we have output on startup
RateLimiter.labels("ip", "GET").inc(0)

# Count of individual validation errors, not "invalid forms"; each of these validation errors
# represents an issue the front end should have caught but didn't.
DonationProcessingError = Counter(
    "donate_form_submission_error_count",
    "Number of errors encountered when processing donation form",
    ["type"]
)
# possible values are the error.code from tordonate.forms
DonationProcessingError.labels("captcha").inc(0)

DonationTransactionCount = Counter(
    "donate_transaction_count",
    "Number of donations validated and sent to CiviCRM",
    ["type", "status"]
)
# possible values here are `single`, `recurring`, and `Completed`, and `Failed`
DonationTransactionCount.labels("single", "Completed").inc(0)

WebhookValidation = Counter(
    "donate_webhook_message_count",
    "Number of webhook messages received",
    ["status", "vendor"]
)
# possible values are "failure", "success" and (currently) "stripe" and "paypal"
WebhookValidation.labels("success", "stripe").inc(0)

PaypalIPNValidation = Counter(
    "donate_paypal_ipn_message_count",
    "Number of legacy Paypal IPN messages received",
    ["status"]
)
# possible values are "failure" and "success"
PaypalIPNValidation.labels("success").inc(0)

# Successful webhook messages do not always imply successful transactions, and
# not all transactions are important to report about to CiviCRM; this counter
# therefore tracks all successfully-reported-upon transactions which passed
# initial form validation, but which eventually were rejected by the payment
# processor - often indicating cards with a high fraud warning level, which at
# volume probably indicates card testing.
DonationVendorTransactionCount = Counter(
    "donate_vendor_transaction_count",
    "Number of transactions processed by vendor",
    ["status", "type", "vendor"]
)
# possible values are "failure" and "success", "single" and "recurring",
# and (currently) "stripe" and "paypal"
DonationVendorTransactionCount.labels("success", "single", "stripe").inc(0)
