from django.test import TestCase

from tordonate.utils import get_client_ip


class UtilsTests(TestCase):
    def test_get_client_ip(self) -> None:
        no_proxy = {
            "REMOTE_ADDR": "10.0.0.1",
        }
        self.assertEqual(get_client_ip(no_proxy), "10.0.0.1")
        good_proxy = {
            "REMOTE_ADDR": "10.0.0.1",
            "HTTP_X_FORWARDED_FOR": "10.0.0.2",
        }
        self.assertEqual(get_client_ip(good_proxy), "10.0.0.2")
        bad_proxy = {
            "REMOTE_ADDR": "10.0.0.1",
            "HTTP_X_FORWARDED_FOR": "10.0.0.3, 10.0.0.2",
        }
        self.assertEqual(get_client_ip(bad_proxy), "10.0.0.2")
