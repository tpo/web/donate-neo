from unittest.mock import MagicMock

from django.test import Client, TestCase

from tordonate import container
from tordonate.civicrm.forms import SubscriptionForm
from tordonate.civicrm.repository import CivicrmRepositoryMock


class SubscriptionFormTests(TestCase):
    newsletter_data = {
        "email_address": "address@email.com",
        # captcha_0 is the captcha image, captcha_1 is the user-input captcha text;
        # when they both return "PASSED" then the user has entered the correct text
        "captcha_0": "PASSED",
        "captcha_1": "PASSED",
    }

    def test_newsletter_signup_validation(self) -> None:
        form = SubscriptionForm(data=self.newsletter_data)
        self.assertTrue(form.is_valid())

    def test_newsletter_signup_invalid_email(self) -> None:
        form = SubscriptionForm(
            data={
                **self.newsletter_data,
                "email_address": "malformed@email",
            }
        )
        self.assertFalse(form.is_valid())
        self.assertIsNotNone(form.errors.get("__all__"))

    def test_newsletter_signup_invalid_captcha(self) -> None:
        form = SubscriptionForm(
            data={
                **self.newsletter_data,
                "captcha_1": "FAILED",
            }
        )
        self.assertFalse(form.is_valid())

    def test_newsletter_signup_post_results(self) -> None:
        client = Client()
        civi_mock = MagicMock(spec=CivicrmRepositoryMock)
        civi_mock.newsletter_signup_sync = MagicMock()
        civi_mock.newsletter_signup_sync.side_effect = None
        with container.civi.override(civi_mock):
            response = client.post(
                '/subscribe/',
                follow=True,
                data=self.newsletter_data
            )
            self.assertEqual(response.status_code, 200)
            self.assertTrue(civi_mock.newsletter_signup_sync.called)

    def test_newsletter_signup_post_results_failed(self) -> None:
        client = Client()
        civi_mock = MagicMock(spec=CivicrmRepositoryMock)
        civi_mock.newsletter_signup_sync = MagicMock()
        civi_mock.newsletter_signup_sync.side_effect = None
        with container.civi.override(civi_mock):
            response = client.post(
                '/subscribe/',
                follow=True,
                data={
                    **self.newsletter_data,
                    "captcha_1": "FAILED",
                }
            )
            self.assertEqual(response.status_code, 200)
            self.assertFalse(civi_mock.newsletter_signup_sync.called)
