from django.test import TestCase

from tordonate.containers import Container
from tordonate.forms import DonorInfoForm


# When testing the donation form, be mindful of the fact that it expresses donation values in
# whole dollars, whereas the rest of the infrastructure keeps dollar amounts in cents.
# It is good practice to refrain from using hardcoded values in these tests, so that changing
# donation and perk minimums are automatically reflected in these test cases, so
# be mindful of how DonorInfoFormTests.setUp pulls values directly from civicrm.repository
# (which itself gets them fresh from source) and converts them for proper use in testing here.
class DonorInfoFormTests(TestCase):
    data = {
        "custom_donation": 25,
        "single_or_monthly": "single",
        "no_gift": True,
        "first_name": "first",
        "last_name": "last",
        "street_address": "street address",
        "apartment_number": "apartment",
        "country": "country",
        "city": "city",
        "state": "state",
        "postal_code": "90210",
        "email_address": "address@email.com",
        "email_updates": False,
        # captcha_0 is the captcha image, captcha_1 is the user-input captcha text;
        # when they both return "PASSED" then the user has entered the correct text
        "captcha_0": "PASSED",
        "captcha_1": "PASSED",
    }

    def setUp(self) -> None:
        di_container = Container()
        self.civi = di_container.civi()
        # For testing custom_donation amounts at or just around minimum values
        # (because, again, data.custom_donation is an expression of dollars
        # and most stored money values are expressed as cents).
        # We ignore typing on these calls because Mypy gets confused by the conversion performed
        # by tordonate.utils.WithSyncMeta - which provides the below sync versions of async calls.
        # (This is not the only place where typing on these values is enforced, and if they are
        # incorrect then the build will not pass testing, so this is not as unsafe as it appears.)
        self.minimum_donation = self.civi.minimum_donation_sync() / 100  # type: ignore
        self.donation_perk_single_minimum = self.civi.get_active_perks_sync()[  # type: ignore
            0
        ].single_price / 100
        self.donation_perk_monthly_minimum = self.civi.get_active_perks_sync()[  # type: ignore
            0
        ].monthly_price / 100

    def test_form_validation(self) -> None:
        form = DonorInfoForm(data=self.data)
        self.assertTrue(form.is_valid())

    def test_form_invalid_email(self) -> None:
        form = DonorInfoForm(
            data={
                **self.data,
                "email_address": "malformed@email",
            }
        )
        self.assertFalse(form.is_valid())
        self.assertIsNotNone(form.errors.get("__all__"))

    def test_form_minimum_donation(self) -> None:
        form = DonorInfoForm(
            data={
                **self.data,
                "custom_donation": self.minimum_donation,
            }
        )
        self.assertTrue(form.is_valid())
        self.assertIsNone(form.errors.get("__all__"))

    def test_form_insufficient_donation_amount(self) -> None:
        form = DonorInfoForm(
            data={
                **self.data,
                "custom_donation": self.minimum_donation - 1,
            }
        )
        self.assertFalse(form.is_valid())
        self.assertIsNotNone(form.errors.get("__all__"))

    def test_form_invalid_donation_amount(self) -> None:
        form = DonorInfoForm(
            data={
                **self.data,
                "custom_donation": "five dollars",
            }
        )
        self.assertFalse(form.is_valid())
        self.assertIsNotNone(form.errors.get("__all__"))

    def test_form_valid_donation_amount_for_perk(self) -> None:
        form = DonorInfoForm(
            data={
                **self.data,
                "custom_donation": self.donation_perk_single_minimum,
                "no_gift": False,
                "perk": self.civi.get_active_perks_sync()[0].id,  # type: ignore
            }
        )
        self.assertTrue(form.is_valid())
        self.assertIsNone(form.errors.get("__all__"))

    def test_form_invalid_donation_amount_for_perk(self) -> None:
        form = DonorInfoForm(
            data={
                **self.data,
                "custom_donation": self.donation_perk_single_minimum - 1,
                "no_gift": False,
                "perk": self.civi.get_active_perks_sync()[0].id,  # type: ignore
            }
        )
        self.assertFalse(form.is_valid())
        self.assertIsNotNone(form.errors.get("__all__"))

    def test_form_invalid_perk(self) -> None:
        form = DonorInfoForm(
            data={
                **self.data,
                "custom_donation": self.minimum_donation,
                "no_gift": False,
                "perk": "",
            }
        )
        self.assertFalse(form.is_valid())
        self.assertIsNotNone(form.errors.get("__all__"))

    def test_form_monthly_donation(self) -> None:
        form = DonorInfoForm(
            data={
                **self.data,
                "single_or_monthly": "monthly",
                "custom_donation":
                    self.donation_perk_monthly_minimum,
                "no_gift": False,
                "perk": self.civi.get_active_perks_sync()[0].id,  # type: ignore
            }
        )
        self.assertTrue(form.is_valid())
        self.assertIsNone(form.errors.get("__all__"))

    def test_form_insufficient_monthly_donation(self) -> None:
        form = DonorInfoForm(
            data={
                **self.data,
                "single_or_monthly": "monthly",
                "custom_donation":
                    self.donation_perk_monthly_minimum - 1,
                "no_gift": False,
                "perk": self.civi.get_active_perks_sync()[0].id,  # type: ignore
            }
        )
        self.assertFalse(form.is_valid())
        self.assertIsNotNone(form.errors.get("__all__"))

    def test_no_perk_and_not_no_gift(self) -> None:
        form = DonorInfoForm(
            data={
                **self.data,
                "no_gift": False,
            }
        )
        self.assertFalse(form.is_valid())
        self.assertIsNotNone(form.errors.get("__all__"))

    def test_perk_and_no_gift(self) -> None:
        form = DonorInfoForm(
            data={
                **self.data,
                "custom_donation": self.donation_perk_single_minimum,
                "no_gift": True,
                "perk": self.civi.get_active_perks_sync()[0].id,  # type: ignore
            }
        )
        self.assertFalse(form.is_valid())
        self.assertIsNotNone(form.errors.get("__all__"))

    def test_restricted_shipping(self) -> None:
        form = DonorInfoForm(
            data={
                **self.data,
                "country": "Russia"
            }
        )
        self.assertFalse(form.is_valid())
        self.assertIsNotNone(form.errors.get("__all__"))
