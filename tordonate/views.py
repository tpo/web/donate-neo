# mypy: disable-error-code=no-untyped-def
import base64
import binascii
import hashlib
import hmac
import json
import logging
import secrets
import time

from dependency_injector.wiring import Provide, inject
from django.conf import settings
from django.http import HttpRequest, HttpResponse, HttpResponseNotAllowed, JsonResponse, QueryDict
from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import FormView, TemplateView

from . import prometheus
from .containers import Container
from .forms import CryptocurrencyForm, DonorInfoForm
from .utils import create_order_summary, get_client_ip

logger = logging.getLogger(__package__)


class DonateFormView(FormView):
    form_class = DonorInfoForm
    template_name = "donate.html.jinja"
    success_url = reverse_lazy("donate_thank_you")

    def get_context_data(self, civi=Provide[Container.civi], **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(
            {
                "donate_single_prices": settings.DONATE_PRICES["single"],
                "donate_monthly_prices": settings.DONATE_PRICES["monthly"],
                "paypal_client_id": settings.PAYPAL_CLIENT_ID,
                "stripe_api_key": settings.STRIPE_API_KEY,
                "crypto_wallets": settings.CRYPTO_WALLET_ADDRESSES,
                "is_yec": civi.is_yec_sync(),
            }
        )

        return context

    @inject
    def form_valid(self, form: DonorInfoForm, civi=Provide[Container.civi]):
        self.request.session["order_summary"] = create_order_summary(form)
        civi.report_donation_sync(form)
        return super().form_valid(form)

    @inject
    def ajax_validate(
        request: HttpRequest,
        civi=Provide[Container.civi],
    ) -> HttpResponse:
        if request.method != "POST":
            return HttpResponseNotAllowed(["POST"])

        jsonData = json.loads(request.body)
        data = QueryDict(jsonData["form"].encode("ASCII"))
        form = DonorInfoForm(data)

        if form.is_valid():
            if form.cleaned_data["email_updates"]:
                civi.newsletter_signup_sync({
                    "email_address": form.cleaned_data["email_address"],
                    "first_name": form.cleaned_data["first_name"],
                    "last_name": form.cleaned_data["last_name"],
                })
            return JsonResponse({})
        else:
            # Log IP of users who fail form validation, for cases of one IP causing bulk failures
            client_ip = get_client_ip(request.META)
            logger.error(
                "User submitted form, but its content caused a backend validation error; IP address %s",  # noqa: E501
                client_ip
            )
            return JsonResponse({"errors": form.errors})


class CryptocurrencyView(FormView):
    form_class = CryptocurrencyForm
    template_name = "cryptocurrency.html.jinja"
    success_url = reverse_lazy("donate_thank_you")

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(
            {
                "donate_single_prices": settings.DONATE_PRICES["single"],
                "crypto_wallets": settings.CRYPTO_WALLET_ADDRESSES,
                "btcpayserver_url": settings.BTCPAYSERVER_URL,
            }
        )
        return context


class DonateThankYouView(TemplateView):
    template_name = "donate_thank_you.html.jinja"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context.update(
            {
                "order_summary": self.request.session.pop("order_summary", {}),
                "stripe_api_key": settings.STRIPE_API_KEY,
            }
        )
        return context


class FAQView(TemplateView):
    template_name = "faq.html.jinja"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context


class PrivacyPolicyView(TemplateView):
    template_name = "privacy_policy.html.jinja"


class StateDisclosuresView(TemplateView):
    template_name = "state_disclosures.html.jinja"


class RefundPolicyView(TemplateView):
    template_name = "refund_policy.html.jinja"


def handler404(request, exception):
    return render(request, "errors/404.html.jinja", status=404)


def handler500(request):
    return render(request, "errors/500.html.jinja", status=500)


def RateLimiter(request, exception):
    client_ip = get_client_ip(request.META)
    logger.info("Rate limiter triggered on path {} via method {} by IP {}".format(
        request.path,
        request.method,
        client_ip
    ))
    prometheus.RateLimiter.labels("ip", request.method).inc()
    return HttpResponse("429: Rate Limit Exceeded", status=429)


def sha2_hex(salt, number) -> str:
    hasher = hashlib.sha256()
    hasher.update((salt + str(number)).encode('utf-8'))
    hash_value = hasher.digest()
    return hash_value.hex()


def hmac_hex(secret_key, challenge) -> str:
    hash_algorithm = 'sha256'
    hmac_object = hmac.new(
        secret_key,
        challenge.encode(),
        getattr(hashlib, hash_algorithm)
    )
    return hmac_object.hexdigest()


# AltchaView() contains the backend portion of the implementation of ALTCHA: https://altcha.org/
# The intended flow between the user's browser and this server moves as such:
# 1. A user loads the donate page; the ALTCHA captcha sits in the form where Stripe's card fields
#    normally reside.
# 2. User interaction with the form causes the ALTCHA captcha to request a proof-of-work challenge
#    from /challenge/, which is mapped to AltchaView.serve_challenge().
# 3. The user's browser eventually solves the challenge and returns a payload to /verifychallenge/,
#    which is mapped to AltchaView.verify_challenge(). This method calls
#    AltchaView.test_challenge() to perform the actual work of ensuring the returned payload
#    correctly unpacks into the intended JSON data and reproduces the correct algorithm, and a
#    valid challenge and server signature.
# 4. If AltchaView.verify_challenge() returns 200 OK, the front-end ALTCHA module browser changes
#    state to "verified", and a callback in the Javascript module static/js/modules/stripe.js
#    calls stripePaymentElementReveal(), which hides the ALTCHA module and embeds the Stripe
#    payment fields, allowing a donation transaction to be submitted.
# 5. Upon submission and successful front-end validation, a Stripe transaction is submitted to
#    /stripe/process/. This submission now also includes the aforementioned ALTCHA payload, which
#    is re-verified by AltchaView.test_challenge(). Much as a transaction which fails backend
#    form validation will be rejected, so too will a transaction which fails ALTCHA verification.
class AltchaView():
    def __init__(self):
        # Base64-decode the HMAC server key from easily-storable ASCII back to a bytestring
        self.hmac_bytestring = base64.b64decode(bytes(settings.HMAC_KEY, "utf-8"))

    def test_challenge(self, payload) -> bool:
        if not payload:
            return False
        try:
            data = json.loads(base64.b64decode(payload))
        except (UnicodeDecodeError, binascii.Error, ValueError):
            return False

        # Validate algorithm
        alg_ok = data["algorithm"] == 'SHA-256'

        # Validate challenge
        challenge_ok = data["challenge"] == sha2_hex(data["salt"], str(data["number"]))

        # Validate signature
        signature_ok = data["signature"] == hmac_hex(
            self.hmac_bytestring,
            data["challenge"]
        )

        if alg_ok and challenge_ok and signature_ok:
            return True

        return False

    # Create and serve proof-of-work challenge for front-end captcha
    # https://altcha.org/docs/proof-of-work/
    async def serve_challenge(self, request: HttpRequest) -> JsonResponse:
        # Passing a salt with an "expires" URL parameter sets an expiry time for
        # otherwise-valid challenge payloads, making it impossible to reuse a
        # single solved challenge indefinitely.
        # The current implementation sets expiry time to one hour (3600 seconds).
        salt = secrets.token_urlsafe(10) + "?expires=" + str(int(time.time()) + 3600)

        # The number created here determines the complexity of the PoW mechanism.
        # Adjust bounds upward to increase complexity, decrease bounds to decrease
        # the time it takes to generate PoW payload.
        randval = secrets.choice(range(10000, 25000))

        # Compute SHA256 hash of the concatenated salt + secret_number,
        # then encode results as a HEX string
        challenge = sha2_hex(salt, randval)

        # Create a server signature using a hex-encoded HMAC key
        signature = hmac_hex(
            self.hmac_bytestring,
            challenge
        )

        return JsonResponse(
            {
                "algorithm": 'SHA-256',
                "challenge": challenge,
                "salt": salt,
                "signature": signature,
            },
            status=200,
        )

    # Verify proof-of-work challenge for front-end captcha
    # (Note that we exempt this request from requiring a CSRF token be passed; this is because
    # the front-end CAPTCHA library making this request for validation does not provide a way to
    # pass extra header or body information along with its verification request. However,
    # a request to this route must include a payload containing a solved proof-of-work challenge
    # and can be trivially rejected if that payload does not decrypt into a legitimate UTF-8
    # JSON object [much less one containing valid data]. This serves a similar purpose as the
    # CSRF token in the first place, and verifying this challenge only serves to enable access to
    # site features which themselves require a valid CSRF token to function.)
    @csrf_exempt
    def verify_challenge(self, request: HttpRequest) -> JsonResponse:
        # Reject any request not bearing a message body.
        if getattr(request, "body") is None:
            # Scrapers prodding this endpoint probably shouldn't generate an outright error.
            logger.info("ALTCHA challenge response contained no message.")
            return JsonResponse({}, status=400)

        # Reject any request whose body isn't valid JSON.
        try:
            body = json.loads(request.body)
        except json.JSONDecodeError:
            logger.error("ALTCHA challenge response wasn't valid JSON.")
            return JsonResponse({}, status=400)

        # Reject any request whose body doesn't contain a solution payload.
        if "payload" not in body:
            logger.error("ALTCHA challenge response contained no payload.")
            return JsonResponse({}, status=400)

        # Only consider the request verified if all checks return true.
        if self.test_challenge(body["payload"]):
            return JsonResponse(
                {"payload": body["payload"]},
                status=200,
            )
        logger.error("ALTCHA challenge response submitted but failed.")
        return JsonResponse({}, status=400)


def redirect_view(request, destpath: str = "/"):
    response = redirect(destpath, permanent=True)
    return response
