from django import forms
from django.core.validators import MinValueValidator

from .. import container


class StripeDonationForm(forms.Form):
    recurring = forms.BooleanField(required=False)
    amount = forms.IntegerField(
        required=True,
        validators=[
            MinValueValidator(container.civi().minimum_donation_sync()),  # type: ignore
        ],
    )
    email = forms.EmailField(required=True)
