from django.urls import path

from . import views

urlpatterns = [
    path("process/", views.process, name="process"),
    path("webhook/", views.webhook, name="webhook"),
]
