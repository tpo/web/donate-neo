from dataclasses import dataclass


@dataclass
class Perk:
    id: str
    name: str
    desc: str
    single_price: int
    monthly_price: int
    image_data: str
    options: list
