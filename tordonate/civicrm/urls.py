from django.urls import path

from . import views
from .views import (OptoutView, ResubscriptionView, SubscribedView, SubscriptionRequestedView,
                    SubscriptionView, UnsubscriptionView)

urlpatterns = [
    path("civicrm/mailing/<str:action>", views.mailing_action, name="mailing_action"),
    path("confirm-subscription", views.confirm_subscription, name="confirm_subscription"),
    path(
        "validate-subscription/", SubscriptionView.validate_captcha, name="validate_subscription"
    ),
    path("subscribe/", SubscriptionView.as_view(), name="subscribe"),
    path("subscribed/", SubscribedView.as_view(), name="subscribed"),
    path(
        "subscription-requested/",
        SubscriptionRequestedView.as_view(),
        name="subscription_requested"
    ),
    path("unsubscribe/", UnsubscriptionView.as_view(), name="unsubscribe"),
    path("resubscribe/", ResubscriptionView.as_view(), name="resubscribe"),
    path("optout/", OptoutView.as_view(), name="optout"),
]
