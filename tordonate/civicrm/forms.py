import logging
import typing as t

from captcha.fields import CaptchaField, CaptchaTextInput
from django import forms
from django.core.exceptions import ValidationError

logger = logging.getLogger(__package__)


class CaptchaBetter(CaptchaTextInput):
    template_name = "widgets/captcha_with_audio.html"


class SubscriptionForm(forms.Form):
    email_address = forms.EmailField(required=True, max_length=1000)

    email_address.widget = forms.EmailInput(attrs={"class": "mt-2 form-control"})

    captcha = CaptchaField(widget=CaptchaBetter, label="")

    def clean(self) -> t.Any:
        errors = []
        cleaned_data = super().clean()

        if cleaned_data.get("email_address") is None:
            errors.append(ValidationError("Please enter a valid email address."))

        if len(errors):
            raise ValidationError(errors)
        return cleaned_data
