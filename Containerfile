FROM containers.torproject.org/tpo/tpa/base-images/python:bookworm

ENV PYTHONFAULTHANDLER=1 \
  PYTHONUNBUFFERED=1 \
  PIP_DISABLE_PIP_VERSION_CHECK=on \
  PIP_DEFAULT_TIMEOUT=100 \
  WORKDIR=/home/tordonate/app \
  VIRTUAL_ENV=/home/tordonate/venv \
  PATH="/home/tordonate/venv/bin:$PATH" \
  CSRF_TRUSTED_ORIGIN="http://localhost:8000"

# run-time dependencies
RUN apt-get update && \
  apt-get install -y --no-install-recommends \
    flite \
    sox \
    python-is-python3 \
    python3-poetry && \
  apt-get clean && \
  rm -rf /var/lib/apt/lists/*

# Create unprivileged tordonate user/group
RUN groupadd -r -g 999 tordonate && \
  useradd --no-log-init -r -m -u 999 -g tordonate tordonate

# Work in application directory
WORKDIR /home/tordonate/app

# Expose port 8000/tcp for gunicorn
EXPOSE 8000

# Switch to tordonate user
USER tordonate

# Copy all project files (minus those ignored)
COPY --chown=tordonate:tordonate . .

# Create virtualenv and install tordonate app
# then make home directory owned by the right user
RUN --mount=type=cache,uid=999,gid=999,target=/cache \
  python3 -m venv /home/tordonate/venv && \
  PIP_CACHE_DIR=/cache/pip POETRY_CACHE_DIR=/cache/poetry \
    poetry install --without=dev --no-interaction --no-ansi && \
  poetry run ./manage.py makemigrations

# Launch the app
CMD ["./launch.sh"]
